const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const axios = require('axios');


const app=express();

app.use(express.urlencoded({extended:true}));
app.use(express.json());
app.use(morgan('tiny'));
app.use(cors());

const posts={};

app.get('/',(req,res)=>{
    res.send(posts);
});
app.get('/posts',(req,res)=>{
    res.send(posts);
});

app.post('/event',(req,res)=>{
    console.log('Received event : ',req.body.type);
    handleEvent(req.body);
    res.send('ok');
});

const handleEvent=(event)=>{
    const {type, data}=event;
    if (type==='PostCreated') {
        const {id,title}=data;
        posts[id]={id,title,comments:[]};
    }
    if (type==='CommentCreated') {
        const post=posts[data.postId];
        post.comments.push({...data});
    }
    if (type==='CommentUpdated') {
        const post=posts[data.postId];
        const comment=comments.find(
            comment=>comment.id===data.id);
        comment={...data};
    }
};

app.listen(4002,async()=>{
    console.log('Listen on 4002');
    try {
        const res = await axios.get('http://events-bus:4010/events');
        if (res.data) {
            for (let event of res.data) {
                console.log('Processing event: '+event.type+' '+event.data.id);
                handleEvent(event);
            }
        }
    } catch (error) {
        console.log('Error',error.message);
    }
});
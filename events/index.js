const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const axios = require('axios');


const app=express();

app.use(express.urlencoded({extended:true}));
app.use(express.json());
app.use(morgan('tiny'));
app.use(cors());

const events=[];

app.get('/',(req,res)=>{
res.send('ok');
})
app.post('/event',(req,res)=>{
    const event=req.body;
    events.push(event);
    console.log('event',event);
    Promise.all(
        [axios.post(`http://posts-cluster:4000/event`,event),
        axios.post(`http://comments-cluster:4001/event`,event),
        axios.post(`http://query-cluster:4002/event`,event),
        axios.post(`http://moderation-cluster:4003/event`,event)]
    ).catch((e)=>{
            console.error('ERROR event: ',e.message);
        });
    const status='new event ok';
    res.send({status, event});
});

app.get('/events',(req,res)=>{
    res.send(events);
});

app.listen(4010,()=>{
console.log('Listen on 4010');
});

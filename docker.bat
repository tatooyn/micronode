# crear imagen
docker build -t acamate/events .

# subir imagen
docker push acamate/events

#crear deploy events
kubectl apply -f event.yaml

# crear junto service junto con deploy
kubectl apply -f event.yaml

#vuelve a cargar un contenedor de deployment
kubectl get deployment
kubectl rollout restart deployment posts-depl
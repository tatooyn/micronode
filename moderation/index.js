const express = require('express');
const axios = require('axios');
const morgan = require('morgan');

const app=express();

app.use(express.urlencoded({extended:true}));
app.use(express.json());
app.use(morgan('tiny'));

app.get('/',(req,res)=>{
    res.send('ok');
});
app.post('/event',(req,res)=>{
    console.log('Event: ',req.body.type);
    const {type, data}=req.body;
    if (type==='CommentCreated') {
        data.status=data.content
            .includes('orange')?'rejected':'approved';
        axios.post('http://events-bus:4010/event',{
            type:'CommentModerated',
            data
        }).catch((err)=>console.log('Error',err.message));
    }
    res.send('ok');
});

app.listen(4003,()=>{
console.log('Listen on 4003');
});
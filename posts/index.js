const express=require('express');
const {randomBytes}=require('crypto');
const cors = require('cors');
const axios = require('axios');
const morgan=require('morgan');

const app=express();


app.use(express.urlencoded({extended: true}));
app.use(express.json());
app.use(cors());
app.use(morgan('tiny'));

const posts={};

app.get('/posts',(req,res)=>{
    res.send(posts);
});

app.post('/posts',async(req,res)=>{
    const id=randomBytes(4).toString('hex');
    const {title}=req.body;
    posts[id]={id,title};
    axios.post('http://events-bus:4010/event',{
        type:'PostCreated',
        data:posts[id]
    }).catch(e=>console.log(e.message));
    res.status(201).send(posts[id]);
});

app.post('/event',(req,res)=>{
    console.log('Received event : ',req.body.type);
    res.send('ok');
});

app.listen(4000,()=>{
    console.log('Posts v4');
    console.log('Listening on 4000');
})
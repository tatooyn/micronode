import PostCreate from "./PostCreate";
import PostList from "./PostList";

function App() {
  return (
    <div className="App container">
      <br></br>
      <h1>Posts</h1>
      <PostCreate></PostCreate>
      <h1>Posts</h1>
      <PostList></PostList>
    </div>
  );
}

export default App;

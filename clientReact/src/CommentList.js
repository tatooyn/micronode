import React from 'react';
 const CommentList=({comments})=>{
    
    const commentRender=comments.map(comment=>{
        return <li key={comment.id}>{comment.content}</li>;
    });
    return <div>
        <ul>
            {commentRender}
        </ul>
    </div>;
}

export default CommentList;
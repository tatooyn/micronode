const express = require('express');
const { randomBytes } = require('crypto');
const cors = require('cors');
const axios = require('axios');


const app = express();

app.use(express.urlencoded({extended:true}));
app.use(express.json());
app.use(cors());


const commentsByPostId = {};

// CRUD
app.get('/post/:id/comments',(req,res)=>{
    res.status(200).send(commentsByPostId[req.params.id]||[]);
});
app.post('/post/:id/comments',(req,res)=>{
    const commentId = randomBytes(4).toString('hex');
    const {content}=req.body;
    const comments=commentsByPostId[req.params.id]||[];
    const comment={id:commentId, content, status:'pending'};
    comments.push(comment);
    commentsByPostId[req.params.id]=comments;
    axios.post('http://events-bus:4010/event',{
        type:'CommentCreated',
        data:{
            ...comment,
            postId:req.params.id
        }
    });
    res.status(201).send(comments);
});

app.post('/event',(req,res)=>{
    const {type,data}=req.body;
    console.log('Event : ',type);
    if (type==='CommentModerated') {
        const comments=commentsByPostId[data.idPost]||[];
        const comment=comments.find(
            comment=>comment.id===data.id);
        comment=data;
        axios.post('http://events-bus:4010/event',{
            type:'CommentUpdated',
            comment
        }).catch((err)=>console.log('Error',err.message));
    }
    res.send('ok');
});


app.listen(4001,()=>{
    console.log("listen on 4001");
})
